package com.moob;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class SingleDuelActivity extends Activity {
	public static String TAG="DUELO_A2";
	int maxBar=360;
	float datoActual=0;
	float[] currentValues;
	public Button botonShot;
	public ImageView imagenResultado;
	public TextView tiempo;
	private SensorManager sensorManager;
	private SensorEventListener sensorlistener;
	public int disparoEn=0;
	public float[] subidaTotal=new float[3];
	public float[] maximosTotal=new float[3];
	public float[] minimoTotal=new float[3];
	Handler manejador;
	long beginTime;
	boolean isWaiting=false;
	public boolean armaCargada=false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		botonShot=(Button) findViewById(R.id.btnshot);
		tiempo=(TextView)findViewById(R.id.tiempotext);
		tiempo.setText("0.0s");
		botonShot.setEnabled(false);
		imagenResultado=(ImageView)findViewById(R.id.imagendisparo);
		inicializarlisteners();
		subidaTotal[0]=subidaTotal[1]=subidaTotal[2]=0;
		maximosTotal[0]=maximosTotal[1]=maximosTotal[2]=0;
		minimoTotal[0]=minimoTotal[1]=minimoTotal[2]=10;

	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	public void onClickShot(View target){
		botonShot.setEnabled(false);
		armaCargada=false;
		long nuevotiempo=System.currentTimeMillis();
		long entimer = nuevotiempo-beginTime;		
		if (minimoTotal[0]>-4)
		{
			Log.d(TAG, "Disparo en el bolsillo");
			imagenResultado.setImageResource(R.drawable.error);
		}else{

			subidaTotal[0]=subidaTotal[1]=subidaTotal[2]=0;
			minimoTotal[0]=minimoTotal[1]=minimoTotal[2]=10;
			maximosTotal[0]=maximosTotal[1]=maximosTotal[2]=0;
			tiempo.setText(String.valueOf(entimer)+"s");
			int disparo=disparoEn;	
			switch (disparo) {
			case 0:
				imagenResultado.setImageResource(R.drawable.suelo);
				break;
			case 1:
				imagenResultado.setImageResource(R.drawable.suelo);				
				break;
			case 2:
				imagenResultado.setImageResource(R.drawable.pierna);				
				break;
			case 3:
				imagenResultado.setImageResource(R.drawable.tronco);				
				break;
			case 4:
				imagenResultado.setImageResource(R.drawable.cabeza);				
				break;
			case 5:
				imagenResultado.setImageResource(R.drawable.cielo);				
				break;
			default:
				break;
			}
		}


	}


	public void inicializarlisteners(){
		manejador = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				//	Log.d(TAG, "Enviado mensaje!");
				endTimer();
			}
		};

		sensorlistener=new SensorEventListener() {
			float[] inR = new float[16];
			float[] I = new float[16];
			float[] gravity = new float[3];
			float[] geomag = new float[3];
			float[] orientVals = new float[3];

			@Override
			public void onSensorChanged(SensorEvent event) {	
				switch (event.sensor.getType()) {  
				case Sensor.TYPE_ACCELEROMETER:
					gravity = event.values.clone();
					almacenarSubidaTelefono(event.values);
					break;
				case Sensor.TYPE_MAGNETIC_FIELD:
					geomag = event.values.clone();
					//        Log.d(TAG, "Sensores!!!TYPE_MAGNETIC_FIELD");
					break;
				}
				if (gravity != null && geomag != null) {
					boolean success = SensorManager.getRotationMatrix(inR, I,
							gravity, geomag);
					if (success) {
						SensorManager.getOrientation(inR, orientVals);
						orientVals[0] = (float) Math.toDegrees(orientVals[0]);
						orientVals[1] = (float) Math.toDegrees(orientVals[1]);
						orientVals[2] = (float) Math.toDegrees(orientVals[2]);
						currentValues=orientVals;
						updateProgress(orientVals[1],1);
					}
				}
			}

			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {

			}
		};

	}

	public void registerSensors(){
		sensorManager=(SensorManager) getSystemService(SENSOR_SERVICE);
		sensorManager.registerListener(sensorlistener, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_GAME);
		sensorManager.registerListener(sensorlistener, sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
				SensorManager.SENSOR_DELAY_GAME);

	}

	public void unregisterSensors(){
		sensorManager.unregisterListener(sensorlistener);
		sensorManager=null;

	}

	public void almacenarSubidaTelefono(float[] subida){
		if ((!isWaiting)&&(armaCargada)){
			//		Log.d(TAG, ""+String.valueOf(subida[0])+"] 1["+String.valueOf(subida[1])+"] 2["+String.valueOf(subida[2])+"]");
			for (int i=0;i<3;i++){
				subidaTotal[i]=subidaTotal[i]+subida[i];
				if (maximosTotal[i]<subida[i]){
					//			Log.d(TAG, "pos=["+String.valueOf(i)+"] "+String.valueOf(minimoTotal[i])+"]" );
					maximosTotal[i]=subida[i];
				}
				if (minimoTotal[i]>subida[i]){
					//				Log.d(TAG, "pos=["+String.valueOf(i)+"] "+String.valueOf(minimoTotal[i])+"]" );
					minimoTotal[i]=subida[i];
				}	
				//	Log.d(TAG, "pos=["+String.valueOf(i)+"] "+String.valueOf(maximosTotal[i])+"]" );

			}
		}else{


		}

	}
	public void updateProgress(final float num, final int position){
		this.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				//		Log.d(TID_EX,"data:"+newData);

				switch (position) {
				case 0:
					String d="X["+String.valueOf(currentValues[0])+"] " +
							"Y:["+String.valueOf(currentValues[1])+"]  " +
							"Z:["+String.valueOf(currentValues[2])+"] ";
					//		textoAlmacenado.setText(d);
					//	progresx.setProgress((int) (num+maxBar/2));
					break;
				case 1:
					//	progresy.setProgress((int) (num+maxBar/2));
					if (num>70){
						if (!armaCargada){
							disparoEn=0;
							armaCargada=true;
							imagenResultado.setImageResource(R.drawable.bolsillo);
							beginTimer();
						}

					}else
						if ((20<num)&&(num<70)){
							if (armaCargada){
								disparoEn=1;
							}
						}else
							if ((5<num)&&(num<20)){
								if (armaCargada){
									disparoEn=2;
								}
							}else
								if ((-15<num)&&(num<5)){
									if (armaCargada){
										disparoEn=3;
									}
								}else
									if ((-20<num)&&(num<-15)){
										if (armaCargada){
											disparoEn=4;
										}
									}else
										if (num<-20){
											if (armaCargada){
												disparoEn=5;
											}
										}		
					break;
				case 2:
					//	progresz.setProgress((int) (num+maxBar/2));
					break;
				default:
					break;
				}

			}
		});

	}

	public void beginTimer(){
		if (!isWaiting){
			isWaiting=true;
			subidaTotal[0]=subidaTotal[1]=subidaTotal[2]=0;
			long tiempo=SystemClock.uptimeMillis()+(long) ((Math.random()*3000)+1000);
			manejador.sendMessageAtTime(new Message(), tiempo);
		}


	}

	public void endTimer(){
		if (isWaiting){
			isWaiting=false;
			beginTime = System.currentTimeMillis();
			//			Log.d(TAG,"Timer inicial:"+String.valueOf(beginTime));
			Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(100);	
			botonShot.setEnabled(true);
			this.runOnUiThread(new Runnable() {
				@Override
				public void run() {

				}
			});
		}


	}








	@Override
	protected void onPause() {
		//apagar pantalla
		Log.d(TAG, "onPause");
		unregisterSensors();
		super.onPause();
	}
	@Override
	protected void onRestart() {
		Log.d(TAG,"onRestart");
		super.onRestart();
	}
	@Override
	protected void onResume() {
		//al volver de onpause
		Log.d(TAG, "onResume");
		registerSensors();
		super.onResume();
	}
	@Override
	protected void onStart() {
		Log.d(TAG, "onStart");
		super.onStart();
	}
	@Override
	protected void onStop() {
		//al entrar otra activity
		Log.d(TAG, "onStop");
		super.onStop();
	}


}
