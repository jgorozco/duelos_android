package com.moob.frwk;

public class Disparo {
	String user;
	float timeToShot;
	float maxVelocity;
	float minVelocity;
	int shotin;
	
	public Disparo(String user, float timeToShot, float maxVelocity,
			float minVelocity, int shotin) {
		super();
		this.user = user;
		this.timeToShot = timeToShot;
		this.maxVelocity = maxVelocity;
		this.minVelocity = minVelocity;
		this.shotin = shotin;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public float getTimeToShot() {
		return timeToShot;
	}
	public void setTimeToShot(float timeToShot) {
		this.timeToShot = timeToShot;
	}
	public float getMaxVelocity() {
		return maxVelocity;
	}
	public void setMaxVelocity(float maxVelocity) {
		this.maxVelocity = maxVelocity;
	}
	public float getMinVelocity() {
		return minVelocity;
	}
	public void setMinVelocity(float minVelocity) {
		this.minVelocity = minVelocity;
	}
	public int getShotin() {
		return shotin;
	}
	public void setShotin(int shotin) {
		this.shotin = shotin;
	}
}
